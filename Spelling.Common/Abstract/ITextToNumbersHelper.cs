﻿namespace Spelling.Common.Abstract
{
    public interface ITextToNumbersHelper
    {
        string TextToNumbers(string text);
    }
}