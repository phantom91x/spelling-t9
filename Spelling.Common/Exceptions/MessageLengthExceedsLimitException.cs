﻿using System;

namespace Spelling.Common.Exceptions
{
    public class MessageLengthExceedsLimitException : Exception
    {
        public override string ToString()
        {
            return "Message length greater than 1000 characters";
        }
    }
}