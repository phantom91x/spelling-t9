﻿using System;

namespace Spelling.Common.Exceptions
{
    public class TestCasesExceedsLimit : Exception
    {
        public override string ToString()
        {
            return "The number of test cases is more than 100 or less than 1";
        }
    }
}