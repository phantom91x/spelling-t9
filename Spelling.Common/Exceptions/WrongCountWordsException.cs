﻿using System;

namespace Spelling.Common.Exceptions
{
    public class WrongCountWordsException : Exception
    {
        public WrongCountWordsException(int expectedCount, int actualCount)
        {
            ExpectedCount = expectedCount;
            ActualCount = actualCount;
        }

        public int ExpectedCount { get; }
        public int ActualCount { get; }

        public override string ToString()
        {
            return "Wrong count words";
        }
    }
}