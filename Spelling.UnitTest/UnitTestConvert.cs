using Autofac;
using Spelling.Bll;
using Spelling.Bll.Concrete.Helpers;
using Spelling.Common.Abstract;
using Xunit;

namespace Spelling.UnitTest
{
    public class UnitTestConvert
    {
        public UnitTestConvert()
        {
            IoC.Initialize(new Bll_IoCModule());
        }

        [Fact]
        public void TestTextToNumbers()
        {
            var textInput = "4\nhi\nyes\nfoo  bar\nhello world";
            var textOuput =
                "Case #1: 44 444\nCase #2: 999337777\nCase #3: 333666 6660 022 2777\nCase #4: 4433555 555666096667775553";
            var textConvertHelper = IoC.Instance.Resolve<ITextToNumbersHelper>();
            var result = textConvertHelper.TextToNumbers(textInput);

            Assert.False(!string.Equals(textOuput, result),
                "Conversion is not true");
        }
    }
}