﻿using Autofac;

namespace Spelling.Bll.Concrete.Helpers
{
    /// <summary>
    /// Helper for autofac IoC
    /// </summary>
    public static class IoC
    {
        private static IContainer _container;

        private static readonly object _sync = new object();

        public static IContainer Instance => _container;

        public static void Initialize(params Module[] modules)
        {
            var builder = new ContainerBuilder();
            foreach (var module in modules)
            {
                builder.RegisterModule(module);
            }

            lock (_sync)
            {
                _container = builder.Build();
            }
        }
    }
}