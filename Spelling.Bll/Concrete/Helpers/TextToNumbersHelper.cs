﻿using System;
using System.Linq;
using Spelling.Common.Abstract;
using Spelling.Common.Exceptions;
using Spelling.Common.Models;

namespace Spelling.Bll.Concrete.Helpers
{
    /// <summary>
    /// Helper for convert text to numbers
    /// </summary>
    public class TextToNumbersHelper : ITextToNumbersHelper
    {
        private string StringToNumbers(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return "";
            }

            if (word.Length > 1000)
            {
                throw new MessageLengthExceedsLimitException();
            }

            SymbolModel lastSymbol = null;
            var result = "";
            foreach (var symbol in word.ToLowerInvariant())
            {
                var symbolRes = StorageSymbols.GetSymbolByLetter(symbol);
                if (lastSymbol?.Number == symbolRes.Number)
                {
                    result += " ";
                }

                result += $"{symbolRes.GetStringNumbers()}";
                lastSymbol = symbolRes;
            }

            return result;
        }

        public string TextToNumbers(string text)
        {
            try
            {
                var strings = text.Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.None).Where(t => t.Length > 0)
                    .ToArray();
                var countStrings = Convert.ToInt32(strings.First());
                if (countStrings != strings.Length - 1)
                    throw new WrongCountWordsException(countStrings, strings.Length - 1);
                if (countStrings > 100 || countStrings < 1) throw new TestCasesExceedsLimit();
                var result = "";
                for (var i = 1; i < strings.Length; i++)
                {
                    if (i > 1) result += "\n";
                    result += $"Case #{i}: {StringToNumbers(strings[i])}";
                }

                return result;
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
                throw new WrongFileFormatException();
            }
        }
    }
}